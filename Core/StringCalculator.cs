﻿using System;
using System.Linq;

namespace Core
{
    public class StringCalculator
    {
        public int Sum(string args)
        {
            if (args == string.Empty)
                return 0;
            var separator = new []{',', '\n'};
            var stringWithNumbers = args;

            if (args.Contains("//"))
            {
                var separatorSymbol = args[2];
                separator = new[] {separatorSymbol};
                stringWithNumbers = args.Split('\n')[1];
            }
            
            var numbers = stringWithNumbers.Split(separator);
            return numbers.Sum(PositiveNumbers);
        }

        private int PositiveNumbers(string arg)
        {
            var number = int.Parse(arg);
            if (number < 0)
                throw new Exception();
            return number;
        }
    }
}